CREATE TABLE Persons (
    PersonID int,
    LastName varchar(255),
    FirstName varchar(255),
    Address varchar(255),
    City varchar(255)
);

INSERT INTO Persons (PersonID, LastName, FirstName, Address, City)
VALUES (1, "Trifan", "Tamara", "Castanilor nr.4", "Iasi");

INSERT INTO Persons (PersonID, LastName, FirstName, Address, City)
VALUES (2, "Cehan", "Alin", "Papadiilor 90", "Paris");