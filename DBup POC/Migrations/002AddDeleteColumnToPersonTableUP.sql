ALTER TABLE Persons
ADD FullName varchar(255);

UPDATE Persons
SET FullName = CONCAT(LastName, " ", FirstName);

ALTER TABLE Persons
ADD SecondFullName varchar(255);

UPDATE Persons
SET SecondFullName = CONCAT(LastName, " ", FirstNameds);