INSERT INTO Persons (PersonID, LastName, FirstName, Address, City)
VALUES (3, "Flescan", "Alexandra", "Albinilor", "Cluj");

INSERT INTO Persons (PersonID, LastName, FirstName, Address, City)
VALUES (4, "Dumitrescu", "Ioana", "Romanitelor", "Bacau");

ALTER TABLE Persons
ADD FullAddress varchar(255);

UPDATE Persons
SET FullAddress = CONCAT(City, ", ", Address);