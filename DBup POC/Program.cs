﻿using System;
using System.Linq;
using System.Reflection;
using DbUp;

namespace DBup_POC
{
    class Program
    {
        static int Main(string[] args)
        {
            var connectionString =
                args.FirstOrDefault()
                ?? "Server=127.0.0.1;Database=Master;uid=root;pwd=igBwvZJ3ydLwacvn";
            EnsureDatabase.For.MySqlDatabase(connectionString);
            var upgrader =
                DeployChanges.To
                             .MySqlDatabase(connectionString)
                             .WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly())
                             .LogToConsole()
                             .Build();
            var result = upgrader.PerformUpgrade();
            if (!result.Successful)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(result.Error);
                Console.ResetColor();
#if DEBUG
                Console.ReadLine();
#endif                
                return -1;
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Success!");
            Console.ResetColor();
            return 0;
        }
    }
}
