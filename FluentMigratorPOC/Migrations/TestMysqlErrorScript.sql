BEGIN Transaction
    -- exit if the duplicate key occurs
    DECLARE CONTINUE HANDLER FOR 1062 SELECT 'Duplicate entries' Message; 
    DECLARE EXIT HANDLER FOR SQLEXCEPTION SELECT 'SQLException encountered' Message; 
    
    UPDATE People SET FullAddres34s=Address+' '+City
END$$

DELIMITER ;

CALL TestProcedure();