using FluentMigrator;

namespace FluentMigratorPOC
{
    [Migration(202200000)]
    public class CreateIntialSchema : Migration
    {
        public override void Up()
        {
            Create.Table("People")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("Surname").AsString()
                .WithColumn("FirstName").AsString()
                .WithColumn("Address").AsString()
                .WithColumn("City").AsString();
        }

        public override void Down()
        {
            Delete.Table("People");
        }
    }
}