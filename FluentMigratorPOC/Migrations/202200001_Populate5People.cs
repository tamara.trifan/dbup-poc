using FluentMigrator;

namespace FluentMigratorPOC
{
    [Migration(202200001)]
    public class Populate5People : Migration
    {
        public override void Up()
        {
            Insert.IntoTable("People").Row(new { FirstName = "Tamara", Surname = "Trifan", Address= "Castanilor nr.4", City = "Iasi" });
            Insert.IntoTable("People").Row(new { FirstName = "Alin", Surname = "Cehan", Address= "Papadiilor nr.7", City = "Paris" });
            Insert.IntoTable("People").Row(new { FirstName = "Ioana", Surname = "Dumitrescu", Address= "Romanitelor nr.10", City = "Bacau" });
            Insert.IntoTable("People").Row(new { FirstName = "Irina", Surname = "Andrei", Address= "Albinilor nr.11", City = "Cluj" });
            Insert.IntoTable("People").Row(new { FirstName = "Dan", Surname = "Sturza", Address= "Teilor nr.23", City = "Bucuresti" });
        }

        public override void Down()
        {
            Delete.FromTable("People").Row(new { FirstName = "Tamara",  Surname = "Trifan" });
            Delete.FromTable("People").Row(new { FirstName = "Alin",  Surname = "Cehan" });
            Delete.FromTable("People").Row(new { FirstName = "Ioana",  Surname = "Dumitrescu" });
            Delete.FromTable("People").Row(new { FirstName = "Irina",  Surname = "Andrei" });
            Delete.FromTable("People").Row(new { FirstName = "Dan",  Surname = "Sturza" });
        }
    }
}