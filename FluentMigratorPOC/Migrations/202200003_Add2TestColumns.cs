using System;
using FluentMigrator;
using Microsoft.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace FluentMigratorPOC
{
    [Migration(202200003)]
    public class Add2TestColumns2 : Migration
    {
        public override void Up()
        {
            Alter.Table("People")
                .AddColumn("Column1Test")
                .AsString()
                .Nullable();
            try
            {
                Execute.Script("Migrations\\TestMysqlErrorScript.sql");
            }
            catch (MySqlException  ex)
            {
                Delete.Column("Column1Test").FromTable("People");
            }

            Alter.Table("People")
                .AlterColumn("Column2Test")
                .AsString()
                .NotNullable();
        }

        public override void Down()
        {
            Delete.Column("Column1Test").FromTable("People");
            Delete.Column("Column2Test").FromTable("People");
        }
    }
}