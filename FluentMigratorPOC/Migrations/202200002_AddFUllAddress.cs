using FluentMigrator;

namespace FluentMigratorPOC
{
    [Migration(202200002)]
    public class AddFullAddress : Migration
    {
        public override void Up()
        {
            Alter.Table("People")
                .AddColumn("FullAddress")
                .AsString()
                .Nullable();

            Execute.Sql(@"
                UPDATE People
                SET FullAddress=CONCAT(Address,' ',City)
            ");

            Alter.Table("People")
                .AlterColumn("FullAddress")
                .AsString()
                .NotNullable();
        }

        public override void Down()
        {
            Delete.Column("FullAddress").FromTable("People");
        }
    }
}